import ROOT
import os
import math

#### script to combine fake templates:
# python combine_fake_templates.py wsi1.root wsi2.root --repl wsi1.root wsi2.root wsi3.root wsi4.root
# the inputs (wsi1 and wsi2) will be considered to determine the combined template. Every fake template in the --repl list 
# will be replaced by the combined fake template, scaled to the previous yield.
#Additionaly a fakeCombined folder with the original entries will be written in the tree as well as normalization uncertainties fakenorm_1up/down


def get_channel(input_string, start_characters ='chan_', end_characters='sr'):
    start_index = input_string.find(start_characters)
    end_index = input_string.find(end_characters)
    return input_string[start_index:end_index + len(end_characters)]



def get_inclusive_histogram(list_of_files):

    sum_i = 0

    for wsi_file in list_of_files:
        print 'for incl {}'.format(wsi_file)
        rfile_input = ROOT.TFile.Open(wsi_file)
        _chan = get_channel(wsi_file)
        h_fake = rfile_input.Get(_chan).Get("Fake").Get("nominal")
        try:
            h_temp.Add(h_fake)
        except:
            h_temp = h_fake.Clone()
            h_temp.SetDirectory(0)

        rfile_input.Close()

        
    return h_temp



if __name__ == '__main__':
    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument('input', nargs='+', help='if multiple inputs are provided, they will be added up')
    parser.add_argument('--repl', nargs='+', help='wsis that will be replaced by the combined one')
    args = parser.parse_args()

    h_inclusive = get_inclusive_histogram(args.input)
    
    
    for wsi_file in args.repl:
        print 'to be repl {}'.format(wsi_file)
        rfile_input = ROOT.TFile.Open(wsi_file, "UPDATE")
        _chan = get_channel(wsi_file)

        #copy fake directory to fake original
        tfake = rfile_input.Get(_chan).Get("Fake")
        tfakecp = rfile_input.Get(_chan).mkdir("FakeOriginal")
        keys_j = tfake.GetListOfKeys()
        
        for j in range(0,keys_j.GetSize()):
            key_t = keys_j.At(j)
            obj_j = key_t.ReadObj()
            tfakecp.WriteTObject(obj_j)

        h_fake = rfile_input.Get(_chan).Get("Fake").Get("nominal")
        
        h_temp = h_inclusive.Clone()
        #replace fakes by inclusive shape
        h_temp.Scale(h_fake.Integral()/h_inclusive.Integral())
        

    
        #add the normalisation uncertainty
        h_temp_up = h_temp.Clone()
        h_temp_down = h_temp.Clone()

        h_fake_bin_error_sum = 0

        for bin in range(h_fake.GetNbinsX()):
            bin_error_squared = h_fake.GetBinError(bin+1)*h_fake.GetBinError(bin+1)
            h_fake_bin_error_sum += bin_error_squared
    
        
        unc = math.sqrt(h_fake_bin_error_sum)/h_fake.Integral()

            
        h_temp_up.SetName("fake_norm_1up")
        h_temp_down.SetName("fake_norm_1down")

        h_temp_up.Scale(1+unc)
        h_temp_down.Scale(1+unc)
            
        rfile_input.Get(_chan).Get("Fake").WriteTObject(h_temp_up, "fake_norm_1up")
        rfile_input.Get(_chan).Get("Fake").WriteTObject(h_temp_down, "fake_norm_1down")
        #delete the old fake histo 
        rfile_input.Get(_chan).Get("Fake").Delete(h_fake.GetName())
        rfile_input.Get(_chan).Get("Fake").WriteTObject(h_temp, "nominal", "Overwrite")

        # Close the ROOT file
        rfile_input.Close()
    
  